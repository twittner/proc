module Test where

import Process

loop :: Process String ()
loop = receive >>= \x ->
    case x of
        "1" -> liftIO (print "1") >>
               mailbox >>= send "2" >>
               loop
        "2" -> liftIO (print "2") >>
               mailbox >>= send "3" >>
               loop
        "3" -> liftIO (print "3") >>
               mailbox >>= send "4" >>
               loop
        _   -> return ()


test :: Process String ()
test = spawn loop >>= register "xxx" >>
       find "xxx" >>= maybe (return ()) (send "1")

main :: IO ()
main = run test

