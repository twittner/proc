{-# LANGUAGE GeneralizedNewtypeDeriving, DeriveDataTypeable #-}

module Process (
    Mailbox
  , Process
  , Registry
  , run
  , spawn
  , mailbox
  , send
  , receive
  , register
  , unregister
  , find
  , liftIO
) where

import Control.Monad
import Control.Monad.Reader
import Control.Monad.State
import Control.Applicative
import Control.Concurrent
import Data.IORef
import Data.Dynamic
import qualified Data.Map as M

data Mailbox a = Mailbox {
    inbox :: Chan a
} deriving (Typeable)

newtype Registry = Registry {
    reg :: IORef (M.Map String Dynamic)
}

newtype Process a b = Process {
    unwrap :: ReaderT (Mailbox a) (StateT Registry IO) b
} deriving (Functor, Monad, MonadIO
          , MonadReader (Mailbox a), MonadState Registry)

instance Applicative (Process a) where
    pure  = return
    (<*>) = ap


-- __API__ --


-- | Run Process with implicit Mailbox and Registry.
run :: Process a b -> IO b
run p = join $ exec p <$> newMailbox <*> newRegistry

-- | Run Process in a separate thread with implicit Mailbox
-- which is returned to the caller.
spawn :: Process a b -> Process a (Mailbox a)
spawn p = join $ go <$> registry <*> liftIO newMailbox
    where
        go r m = liftIO (forkIO_ $ exec p m r) >> return m
        forkIO_ f = forkIO $ f >> return ()

-- | Send a message to a Mailbox.
send :: a -> Mailbox a -> Process a ()
send x d = liftIO $ writeChan (inbox d) x

-- | Wait for a new message to arrive at one's own Mailbox.
receive :: Process a a
receive = mailbox >>= liftIO . readChan . inbox

-- | Get own Mailbox.
mailbox :: Process a (Mailbox a)
mailbox = ask

-- | Register the given Mailbox under the given name.
register :: (Typeable b) => String -> Mailbox b -> Process a (Mailbox b)
register s m = join $ change . reg <$> registry
    where
        change r = liftIO (atomicModifyIORef r insert) >> return m
        insert r = (M.insert s (toDyn m) r, r)

-- | Unregister the Mailbox registered by the given name.
unregister :: String -> Process a ()
unregister s = go . reg <$> registry >> return ()
    where
        go r = M.delete s <$> readIORef r

-- ¦ Retrieve the Mailbox by name. This may return Nothing (i) if
-- nothing is registered by that name, or (ii) if the types do not
-- match between the Process looking for this Mailbox and the Mailbox
-- itself.
find :: (Typeable b) => String -> Process a (Maybe (Mailbox b))
find s = reg <$> registry >>= deref >>= getit
    where
        deref r = liftIO (readIORef r)
        getit r = return $ M.lookup s r>>= fromDynamic


-- __Internal__ --


-- | Run Process with given Mailbox and Registry.
exec :: Process a b -> Mailbox a -> Registry -> IO b
exec p = evalStateT . runReaderT (unwrap p)

-- | Get Registry.
registry :: Process a Registry
registry = get

-- | Create a fresh Mailbox.
newMailbox :: IO (Mailbox a)
newMailbox = Mailbox <$> newChan

-- | Create a fresh Registry.
newRegistry :: IO Registry
newRegistry = Registry <$> newIORef M.empty

